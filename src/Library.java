import java.util.ArrayList;

public class Library {

	ArrayList<Books> books;

	public Library() {
		books = new ArrayList<Books>();

	}

	public void addBook(Books book) {
		books.add(book);
	}

	public String borrowBook(String id, String nameBook) {
		String str = null;
		int a = 0;
		for (Books b : this.books) {
			if (b.getName().equals(nameBook)) {
				a = 1;
				if (b.getStatus() == 1) {
					b.setStatus(0);
					b.setId(id);
					str = "borrow Success";
				} else {
					str = "not borrow";
				}

			}

		}
		if (a == 0) {
			str = "no book";

		}
		return str;
	}

	public String returnBook(String id, String nameBook) {
		String str = null;
		for (Books b : this.books) {
			if (b.getid()==(id)) {

				b.setStatus(1);
				b.setId(null);

				str = "Return Success";

			}
			else {
				str = "id Wrong";
			}
			
			

		}
		return str;

	}

}
