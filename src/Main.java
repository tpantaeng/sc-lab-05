import java.util.ArrayList;

public class Main {
	public static void main(String[] args) {

		ArrayList<Books> books = new ArrayList<Books>();

		Books book1 = new Books("MathBook", 123, "saijai  jaidee", 20);
		Books book2 = new Books("ThaiBook", 124, "Somsree ", 30);
		
		
		
		Student s =new Student ("5510405902", "Utsanee Chumthong ", "Science", "Computer Science ");
		System.out.println(s.getId());

		
		
		Library l = new Library();
		l.addBook(book2);
		l.addBook(book1);
		
		System.out.println(l.borrowBook("5510405902", "MathBook"));
		System.out.println(l.borrowBook("5510405901", "MathBook"));
		System.out.println(l.borrowBook("5510405901", "ThaiBook"));
		System.out.println(l.borrowBook("5510405902", "Book"));
		System.out.println(l.returnBook("5510405902", "MathBook"));
		System.out.println(l.returnBook("5510405902", "MathBook"));
		
		

		// Student student = new Student("5510405902", "Utsanee Chumthong ",
		// "Science", "Computer Science");
		// Course sub1 = new Course("418217", "Java", "c", 3);
		// Course sub2 = new Course("418232", "Algor", "c", 3);
		// Linecourse l1 = new Linecourse(sub1, "A");
		// Linecourse l2 = new Linecourse(sub2, "B");

		// ArrayList<Linecourse> linecourses = new ArrayList<Linecourse>();
		// linecourses.add(l1);
		// linecourses.add(l2);

		// Transcript t = new Transcript(student, linecourses);
		// Transcript t = new Transcript(student);
		// t.addLine(sub1, "A");
		// t.addLine(sub2, "B");
		// System.out.print(t);
	}

}
